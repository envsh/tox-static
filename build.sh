#!/bin/sh

# set -e

WORKDIR=$PWD # /builds/envsh/tox-static/
INSTDIR=$PWD/../../_install
VERSION="0.2.8"
NPROC=2

export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$INSTDIR/lib/pkgconfig
export CFLAGS="-fPIC -O2"
export CXXFLAGS="-fPIC"

install_libsodium() {
    git clone --depth=1 --branch=stable https://github.com/jedisct1/libsodium ./libsodium
    cd ./libsodium  # pushd
    ./autogen.sh
    ./configure --prefix="$INSTDIR" --disable-shared
    make install "-j$NPROC"
    cd - # popd
}

install_libvpx() {
    git clone https://chromium.googlesource.com/webm/libvpx
    # cp -a libvpx-git libvpx
    cd libvpx
    ./configure --prefix=$INSTDIR/ --disable-unit-tests --extra-cflags="$CFLAGS" --extra-cxxflags="$CXXFLAGS"
    make -j3 # V=1
    make install
    cd -
}

install_opus() {
    wget http://downloads.xiph.org/releases/opus/opus-1.3.tar.gz
    tar xvzf opus-1.3.tar.gz
    cd opus-1.3
    ./configure --prefix=$INSTDIR/ --disable-shared --disable-doc --disable-extra-programs
    make -j3
    make install
    cd -
}

install_ctoxcore() {
    wget https://github.com/TokTok/c-toxcore/archive/v${VERSION}.tar.gz
    tar zxf v${VERSION}.tar.gz
    pkgname="c-toxcore-$VERSION"
    cd "$pkgname"

    cmake -DCMAKE_INSTALL_PREFIX="$INSTDIR" -DENABLE_SHARED=off \
          -DENABLE_STATIC=on -DENABLE_TEST=off -DAUTOTEST=off \
          -DMUST_BUILD_TOXAV=on -DBUILD_AV_TEST=off .

    make "-j$NPROC" && make install

    cd - # popd
}

link_test() {
    set -x
    gcc  -o lnktst lnktst.c -I$INSTDIR/include -L$INSTDIR/lib -ltoxcore  -lvpx -lsodium -lpthread
    set +x
}

install_libsodium;
install_libvpx;
install_opus;
install_ctoxcore;
link_test;

set -x
file $INSTDIR/lib/libtoxcore.a
ldd $INSTDIR/bin/DHT_bootstrap
ldd ./lnktst

